package com.test.git.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/test")
public class TestController {

	@GetMapping
	public String index() {
		return "Greetings from Spring Boot! Me invento para que haya cambios";
	}

}
