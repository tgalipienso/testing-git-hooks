package com.test.git;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestGitApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestGitApplication.class, args);
	}

	// Añado un cambio en el master que debe ir a test
}
